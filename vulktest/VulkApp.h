#pragma once

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <iostream>
#include <stdexcept>
#include <functional>
#include <cstdlib>
#include <vector>
#include <cstring>
#include <map>
#include <set>
#include <algorithm>
#include <fstream>

#define ANY_GPU
#define NDEBUG

const int WIDTH = 1280;
const int HEIGHT = 720;

static std::vector<char> readFile(const std::string& filename) {
	std::ifstream file(filename, std::ios::ate | std::ios::binary);

	if (!file.is_open()) {
		throw std::runtime_error("failed to open file");
	}

	size_t fileSize = (size_t)file.tellg();
	std::vector<char> buffer(fileSize);
	file.seekg(0);
	file.read(buffer.data(), fileSize);

	file.close();
	return buffer;
}

struct QueueFamilyIndices {
	int graphicsFamily = -1;
	int presentFamily = -1;

	bool isComplete() {
		return graphicsFamily >= 0 && presentFamily >= 0;
	}
};

const std::vector<const char*> validationLayers = {
	"VK_LAYER_LUNARG_standard_validation"
};

const std::vector<const char*> deviceExtensions = {
	VK_KHR_SWAPCHAIN_EXTENSION_NAME
};

#ifndef NDEBUG
const bool enableValidationLayers = false;
#else
const bool enableValidationLayers = true;
#endif

struct SwapChainSupportDetails {
	VkSurfaceCapabilitiesKHR capabilities;
	std::vector<VkSurfaceFormatKHR> formats;
	std::vector<VkPresentModeKHR> presentModes;
};

class VulkApp {

public:
	GLFWwindow * window;

private:
	VkInstance						instance;
	const char*						AppName = "The VulkApp";
	VkDebugReportCallbackEXT		callback;
	VkPhysicalDevice				physicalDevice = VK_NULL_HANDLE;
	VkDevice						device;
	VkQueue							graphicsQueue;
	VkQueue							presentQueue;
	VkSurfaceKHR					surface;
	VkSwapchainKHR					swapChain;
	std::vector<VkImage>			swapChainImages;
	std::vector<VkImageView>		swapChainImageViews;
	VkFormat						swapChainImageFormat;
	VkExtent2D						swapChainExtent;
	VkShaderModule					vertShaderModule;
	VkShaderModule					fragShaderModule;
	VkPipelineLayout				pipelineLayout;
	VkRenderPass					renderPass;
	VkPipeline						graphicsPipeline;
	std::vector<VkFramebuffer>		swapChainFramebuffers;
	VkCommandPool					commandPool;
	std::vector<VkCommandBuffer>	commandBuffers;

	VkSemaphore						imageAvailableSemaphore;
	VkSemaphore						renderFinishedSemaphore;


public:
	void						run();

private:
	void						initWindow();
	void						initVulkan();

	void						mainLoop();
	void						drawFrame();
	void						cleanup();

	void						createInstance();
	void						setupDebugCallback();
	void						createSurface();
	void						pickPhysicalDevice();
	void						createLogicalDevice();
	void						createSwapChain();
	void						createImageViews();
	void						createRenderPass();
	void						createGraphicsPipeline();
	void						createFramebuffers();
	void						createCommandPool();
	void						createCommandBuffers();
	void						createSemaphores();

	VkShaderModule				createShaderModule(const std::vector<char>& code);
	bool						checkValidationLayerSupport();
	std::vector<const char*>	getRequiredExtensions();
	bool						isDeviceSuitable(VkPhysicalDevice device);
	bool						checkDeviceExtensionSupport(VkPhysicalDevice device);
	int							rateDeviceSuitability(VkPhysicalDevice device);
	QueueFamilyIndices			findQueueFamilies(VkPhysicalDevice device);
	SwapChainSupportDetails		querySwapChainSupport(VkPhysicalDevice device);
	VkSurfaceFormatKHR			chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats);
	VkPresentModeKHR			chooseSwapPresentMode(const std::vector<VkPresentModeKHR> availablePresentModes);
	VkExtent2D					chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities);

	static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
		VkDebugReportFlagsEXT flags,
		VkDebugReportObjectTypeEXT objType,
		uint64_t obj,
		size_t location,
		int32_t code,
		const char* layerPrefix,
		const char* msg,
		void* userData);

};